///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World II
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello1.cpp
/// @version 1.0
///
/// @author Ariel Thepsenavong <arielat@hawaii.edu>
// @ date 2/13/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>               

using namespace std;

int main() {                      

   cout << "Hello World!" << endl;  
                                  

   return 0;
}

