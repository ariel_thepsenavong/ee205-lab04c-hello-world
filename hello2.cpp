/// Lab 04c - Hello World II
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello2.cpp
/// @version 1.0
///
/// @author Ariel Thepsenavong <arielat@hawaii.edu>
// @ date 2/13/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {

   std::cout << "Hello World!" << std::endl;

   return 0;
}

